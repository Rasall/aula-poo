package exercicios;

import java.util.Scanner;

import java.lang.Math;

public class ex4 {

	public static void main(String[] args) {
		
		double comp, larg, alt, area, num;
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("Qual o comprimento da cozinha");
		comp = in.nextDouble();
		
		System.out.println("Qual a largura da cozinha");
		larg = in.nextDouble();
		
		System.out.println("Qual a altura da cozinha");
		alt = in.nextDouble();
		
		area = (comp*alt*2) + (comp*alt*2);
		
		num = area/1.5;
		
		System.out.println("Quantidade de caizas para revestir todas as paredes: "+Math.round(num));
	}

}
