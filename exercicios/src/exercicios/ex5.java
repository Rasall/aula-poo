package exercicios;

import java.util.Scanner;

public class ex5 {

	public static void main(String[] args) {
		
		double odoi,
		odof,
		litro,
		total,
		media,
		lucro,
		valor;
		
		valor = 4.80;
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("Marcacao inicial do odometro (Km): ");
		odoi = in.nextDouble();
		
		System.out.println("Marcacao final do odometro (Km): ");
		odof = in.nextDouble();
		
		System.out.println("Quantidade de combustivel gasto (litros): ");
		litro = in.nextDouble();
		
		System.out.println("Valor total recebido (R$): ");
		total = in.nextDouble();
		
		media = (odof-odoi)/litro;
		lucro = total - (litro * valor);
		
		System.out.printf("\n"+ "Media de consumo em Km/L: %.2f",media);
		System.out.printf("\n"+"Lucro (liquido) do dia: %.2f ",lucro);
	}

}
