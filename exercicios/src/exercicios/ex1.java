package exercicios;

import java.util.Scanner;

public class ex1 {

	public static void main(String[] args) {
		
		double raio,area;
		final double pi = 3.14;
		Scanner in = new Scanner(System.in);
		
		System.out.println("Insira o raio do circulo:");
		raio= in.nextDouble();
		
		area = (raio * raio )*pi;
		
		System.out.printf("A area do circulo � %.2f metros",area);
	}

}
