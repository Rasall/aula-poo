package exercicios;

import java.util.Scanner;

import java.lang.Math;

public class ex3 {

	public static void main(String[] args) {
		double potl,//Potencia da lampada
		larg, //largura do comodo
		comp, // comprimento do comodo 
		area, //area do comodo
		pott, //potencia total
		num; // numero de lampada
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("Insira a pot�ncia da l�mpada em watts:");
		potl = in.nextDouble();
		
		System.out.println("Insira a largura do c�modo em metros:");
		larg = in.nextDouble();
		
		System.out.println("Insira o comprimento do c�modo em metros:");
		comp = in.nextDouble();
		
		area = larg*comp;
		pott = area * 18;
		num = Math.round(pott /potl);
		
		System.out.println("Numero de l�mpadas necess�rio para iluminar esse c�modo:"+ num);
	}

}
